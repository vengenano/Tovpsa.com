//show menu 
$(document).ready(function() {
   $(document).ready(function(){
    $("#showMenu").click(function(){
        $(".d-md-flex").toggle('slow')
        .style.display = 'none';
    });
  });
});

//Check validate form
$(document).ready(function() {
  function validate () {
  var Uname = document.getElementById("username").value;
  var Email = document.getElementById("email").value;
  var Password = document.getElementById("password").value;
  var confpassword = document.getElementById("confirm-password").value;

  if (Uname == "" ) {
    document.getElementById("stCheck").innerHTML = "* Invalid Student Name can not be empty!";
    document.getElementById("username").style.border = "1px solid red";
    document.getElementById("username").focus();
    return false;
  }else {
    document.getElementById("username").innerHTML = "";
    document.getElementById("username").style.border = "1px solid green";
  }
  if ((Uname.length) <=1 || (Uname.length) >20) {
    document.getElementById("Uname").innerHTML = "* Invalid Student Name too long, can be (2-20)character";
    document.getElementById("username").style.border = "1px solid red";
    document.getElementById("username").focus();
    return false;
  }else {
    document.getElementById("Uname").innerHTML = "";
    document.getElementById("username").style.border = "1px solid green";
  }
  if(Uname.search(/[^a-zA-Z]+/) !== -1 ){
    document.getElementById("Uname").innerHTML = "* Invalid Only charater!, not include number, or space between";
    document.getElementById("username").style.border = "1px solid red";
    document.getElementById("username").focus();
    return false;
  }else {
    document.getElementById("Uname").innerHTML = "";
    document.getElementById("username").style.border = "1px solid green";
  }

  /////////////////////
  if (school == "" ) {
    document.getElementById("schCheck").innerHTML = "* Invalid School can not be empty!";
    document.getElementById("school").style.border = "1px solid red";
    document.getElementById("school").focus();
    return false;
  }else {
    document.getElementById("schCheck").innerHTML = "";
    document.getElementById("school").style.border = "1px solid green";
  }
  if (school.search(/[^a-zA-Z]+/) !== -1) {
    document.getElementById("schCheck").innerHTML = "* Invalid only charater!, not include number, or space between";
    document.getElementById("school").style.border = "1px solid red";
    document.getElementById("school").focus();
    return false;
  }
  else {
    document.getElementById("schCheck").innerHTML = "";
    document.getElementById("school").style.border = "1px solid green";
  }

  //////////////////////
  if (contact == "" ) {
    document.getElementById("conCheck").innerHTML = "* Invalid Contact can not be empty!";
    document.getElementById("contact").style.border = "1px solid red";
    document.getElementById("contact").focus();
    return false;
  }else {
    document.getElementById("conCheck").innerHTML = "";
    document.getElementById("contact").style.border = "1px solid green";
  }
  if (contact.search(/^(?:[0-9]\d*|\d)$/)) {
    document.getElementById("conCheck").innerHTML = "* Invalid Only number!, not between space";
    document.getElementById("contact").style.border = "1px solid red";
    document.getElementById("contact").focus();
    return false;
  }
  else {
    document.getElementById("conCheck").innerHTML = "";
    document.getElementById("contact").style.border = "1px solid green";
  }
  if (contact.length < 9 ) {
    document.getElementById("conCheck").innerHTML = "* Invalid number has at be least 9 character length!";
    document.getElementById("contact").style.border = "1px solid red";
    document.getElementById("contact").focus();
    return false;
  }
  else {
    document.getElementById("conCheck").innerHTML = "";
    document.getElementById("contact").style.border = "1px solid green";
  }

}

});