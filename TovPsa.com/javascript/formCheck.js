//javascript code
var idTbl = 1;
var list = document.getElementsByTagName("tr");
function validate () {
	var stuForm = document.getElementById("stname").value;
	var school = document.getElementById("school").value;
	var contact = document.getElementById("contact").value;
	if (stuForm == "" ) {
		document.getElementById("stCheck").innerHTML = "* Invalid Student Name can not be empty!";
		document.getElementById("stname").style.border = "1px solid red";
		document.getElementById("stname").focus();
		return false;
	}else {
		document.getElementById("stCheck").innerHTML = "";
		document.getElementById("stname").style.border = "1px solid green";
	}
	if ((stuForm.length) <=1 || (stuForm.length) >20) {
		document.getElementById("stCheck").innerHTML = "* Invalid Student Name too long, can be (2-20)character";
		document.getElementById("stname").style.border = "1px solid red";
		document.getElementById("stname").focus();
		return false;
	}else {
		document.getElementById("stCheck").innerHTML = "";
		document.getElementById("stname").style.border = "1px solid green";
	}
	if(stuForm.search(/[^a-zA-Z]+/) !== -1 ){
		document.getElementById("stCheck").innerHTML = "* Invalid Only charater!, not include number, or space between";
		document.getElementById("stname").style.border = "1px solid red";
		document.getElementById("stname").focus();
		return false;
	}else {
		document.getElementById("stCheck").innerHTML = "";
		document.getElementById("stname").style.border = "1px solid green";
	}
	if (school == "" ) {
		document.getElementById("schCheck").innerHTML = "* Invalid School can not be empty!";
		document.getElementById("school").style.border = "1px solid red";
		document.getElementById("school").focus();
		return false;
	}else {
		document.getElementById("schCheck").innerHTML = "";
		document.getElementById("school").style.border = "1px solid green";
	}
	if (school.search(/[^a-zA-Z]+/) !== -1) {
		document.getElementById("schCheck").innerHTML = "* Invalid only charater!, not include number, or space between";
		document.getElementById("school").style.border = "1px solid red";
		document.getElementById("school").focus();
		return false;
	}
	else {
		document.getElementById("schCheck").innerHTML = "";
		document.getElementById("school").style.border = "1px solid green";
	}
	if (contact == "" ) {
		document.getElementById("conCheck").innerHTML = "* Invalid Contact can not be empty!";
		document.getElementById("contact").style.border = "1px solid red";
		document.getElementById("contact").focus();
		return false;
	}else {
		document.getElementById("conCheck").innerHTML = "";
		document.getElementById("contact").style.border = "1px solid green";
	}
	if (contact.search(/^(?:[0-9]\d*|\d)$/)) {
		document.getElementById("conCheck").innerHTML = "* Invalid Only number!, not between space";
		document.getElementById("contact").style.border = "1px solid red";
		document.getElementById("contact").focus();
		return false;
	}
	else {
		document.getElementById("conCheck").innerHTML = "";
		document.getElementById("contact").style.border = "1px solid green";
	}
	if (contact.length < 9 ) {
		document.getElementById("conCheck").innerHTML = "* Invalid number has at be least 9 character length!";
		document.getElementById("contact").style.border = "1px solid red";
		document.getElementById("contact").focus();
		return false;
	}
	else {
		document.getElementById("conCheck").innerHTML = "";
		document.getElementById("contact").style.border = "1px solid green";
	}
//add data to table
	var col = {id : idTbl, name : document.querySelector("#stname").value,gender : document.querySelector("#gender").value,
	    school : document.querySelector("#school").value, contactNum : document.querySelector("#contact").value};
    idTbl++;
	var tr = document.createElement("tr");
	tr.setAttribute("onclick", "this.style.backgroundColor = !(this.style.backgroundColor)?'green':null;");
	for (var key in col) {
	    var td = document.createElement("td");
	    td.appendChild(document.createTextNode(col[key]));
	    tr.appendChild(td);
	  }
	var element = document.querySelector("table");
	  element.appendChild(tr);
	  numOfRecords(list.length);
}
function numOfRecords (number) {
	document.querySelector("#record").innerHTML = number-1;
}
//Delete Data form table
function remove() {
  var tbl = document.querySelector("table");
  var arr = [];
  for (var i = 0; i < list.length; i++) {
    if (list[i].style.backgroundColor === "green") {
      arr.push(list[i]);
    }
  }
  //Confirm to delete
  var check = confirm("do you want to delete?");
  if (check==true) {
    arr.forEach(tr => tbl.removeChild(tr));
    list = document.getElementsByTagName("tr");
    numOfRecords(list.length);
  } 
}
